import React from "react";
import ReactDom from "react-dom";

import Button from "../../ui/Button";
import './style.css'

const Modal = (props) => {
    return ReactDom.createPortal(
        <div className="overlay">
            <main className="modal-content">
               <div className="modal-body">
                    <h1>Confirm</h1>
                    <div className="buttons-wrapper">
                        <Button type='ghost' onClick={props.confirmDelete}>&#10003;</Button>
                        <Button type='delete-btn' onClick={()=> props.setIsModalOpen(false)} >X</Button>
                    </div>
                </div>
            </main>
        </div>,
        document.getElementById('modal')
    );
};

export default Modal;
