import React from "react";
import PropTypes from 'prop-types'

import Button from "../../ui/Button";
import Input from "../../ui/Input"

import './style.css'

const UsersTable = (props) => {
    return (
        <table className='user-table'>
            <tbody>
                {props.data.map((u) => {
                    return (<>
                        {props.editing && u.id === props.editUserData.id ? <tr key={u.id}>
                            <td>
                                <Input
                                    className='input-main'
                                    name='name'
                                    type='text'
                                    placeholder='name'
                                    value={props.editUserData.name}
                                    onInputChange={props.onInputChange} />
                            </td>
                            <td>
                                <Input
                                    className='input-main'
                                    name='email'
                                    type='email'
                                    placeholder='email'
                                    value={props.editUserData.email}
                                    onInputChange={props.onInputChange} />
                            </td>
                            <td>
                                <Input
                                    className='input-main'
                                    name='phone'
                                    type='tel'
                                    placeholder='phone'
                                    value={props.editUserData.phone}
                                    onInputChange={props.onInputChange} />
                            </td>
                            <td>
                                <Button type='ghost' onClick={() => props.editConfirm(props.editUserData)}>&#10003;</Button>
                                <Button type='delete-btn' onClick={props.editCancel}>x</Button>
                            </td>
                        </tr> : <tr key={u.id}>
                            <td>{u.name}</td>
                            <td>{u.email}</td>
                            <td>{u.phone}</td>
                            <td>
                                <Button onClick={() => props.editStart(u)} type='edit-btn'>e</Button>
                                <Button onClick={() => props.deleteData(u.id)} type='delete-btn'>x</Button>
                            </td>
                        </tr>}
                    </>
                    )
                })}
            </tbody>
        </table>

    )
};

UsersTable.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    editing: PropTypes.bool.isRequired,
    deleteData: PropTypes.func.isRequired,
    editUserData: PropTypes.object,
    editStart: PropTypes.func.isRequired,
    editConfirm: PropTypes.func.isRequired,
    editCancel: PropTypes.func.isRequired,
    onInputChange: PropTypes.func
};

UsersTable.defaultProps = {
    data: [],
    editing: false,
    editUserData: {
        name: '',
        email: '',
        phone: ''
    }
};


export default UsersTable;