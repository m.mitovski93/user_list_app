import React from "react";
import PropTypes from 'prop-types';

import './style.css';

const Button = (props) => {
    return <button onClick={props.onClick} className={props.type}>{props.children}</button>
};

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
};

Button.defaultProps = {
    type: '',
};

export default Button;