import React from 'react';

import PropTypes from 'prop-types';
import './style.css'

const Input = (props) => {
    return (
        <div>
            <label className='label-main'>{props.label}</label>
            <input className={props.className} type={props.type}
            name={props.name} placeholder={props.placeholder} value={props.value}
            onChange={props.onInputChange} />
        </div>
    )
};

Input.propTypes = {
    label:PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    value: PropTypes.string,
    name:PropTypes.string,
    onInputChange:PropTypes.func
};

Input.defaultProps = {
    label:'',
    type:'',
    placeholder:'',
    className:'',
    value:'',
    name:'',
};
export default Input;