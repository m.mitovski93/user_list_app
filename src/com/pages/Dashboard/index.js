import React from "react";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import AddUserBox from "../../templates/AddUserBox";
import PublicTemplate from "../../templates/PublicTemplate";
import Input from "../../ui/Input";
import Button from "../../ui/Button";
import UsersTable from "../../widgets/UsersTable";
import Modal from "../../widgets/Modal";

import { addUser, deleteUser, editUser } from "../../../services/redux/ducks/users";
import { sanitaze } from "../../../services/util/strings";

import './style.css'

const Dashboard = () => {

    const initState = {
        id: null,
        name: '',
        email: '',
        phone: ''
    };
    const [user, setUser] = useState(initState);
    const [editUserData, setEditUserData] = useState(initState);
    const [editing, setEditing] = useState(false);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [singleId, setSingleId] = useState(null);

    const dispatch = useDispatch();
    const users = useSelector(state => state.users.data);


    const onAddChange = (e) => {
        setUser({ ...user, [e.target.name]: e.target.value, id: users.length + 1 });
    };

    const onEditChange = (e) => {
        setEditUserData({ ...editUserData, [e.target.name]: e.target.value });
    }

    const add = () => {
        const sanitazeInputs = sanitaze(user);
        if (!sanitazeInputs) {
            return alert('Can not add empty fields');
        };

        dispatch(addUser(user));
        setUser(initState);
    };

    const deleteData = (id) => {
        setSingleId(id);
        setIsModalOpen(true);
    };

    const confirmDelete = () => {
        dispatch(deleteUser(singleId));
        setSingleId(null);
        setIsModalOpen(false);
    };

    const editStart = (user) => {
        setEditing(true);
        setEditUserData(user);
    };

    const editConfirm = (userData) => {
        const sanitazeInputs = sanitaze(userData);
        if (!sanitazeInputs) {
            return alert('Can not add empty fields');
        };
        dispatch(editUser(userData));
        setEditing(false);
        setEditUserData(initState);
    };

    const editCancel = () => {
        setEditing(false);
        setEditUserData(initState);
    };


    return (
        <PublicTemplate>
            {isModalOpen && <Modal setIsModalOpen={setIsModalOpen} confirmDelete={confirmDelete} />}
            <div className="dashboard-header">
                <AddUserBox>
                    <Input
                        className='input-main'
                        name='name'
                        type='text'
                        placeholder='name'
                        label='Name'
                        value={user.name}
                        onInputChange={onAddChange} />

                    <Input
                        className='input-main'
                        name='email'
                        type='email'
                        placeholder='email'
                        label='Email'
                        value={user.email}
                        onInputChange={onAddChange} />

                    <Input
                        className='input-main'
                        name='phone'
                        type='tel'
                        placeholder='phone'
                        label='Phone'
                        value={user.phone}
                        onInputChange={onAddChange} />

                    <Button type='save-btn' onClick={add}>Save</Button>
                </AddUserBox>
            </div>
            <div className="dashboard-body">
                <UsersTable
                    data={users}
                    deleteData={deleteData}
                    editing={editing}
                    editUserData={editUserData}
                    editStart={editStart}
                    editConfirm={editConfirm}
                    editCancel={editCancel}
                    onInputChange={onEditChange}
                />
            </div>

        </PublicTemplate>


    )
};

export default Dashboard;