import React from "react";

import './style.css'

const PublicTemplate = (props) => {
    return (<section className='public-template'>
        {props.children}
    </section>)
};

export default PublicTemplate