import React from "react";

import './style.css';

const AddUserBox = (props) => {
    return <div className="add-user-box">
        {props.children}
    </div>
};

export default AddUserBox;