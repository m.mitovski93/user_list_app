import { combineReducers, createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";

import { usersReducer } from "./ducks/users";

const reducers = combineReducers({
    users:usersReducer
});

const middleware = applyMiddleware(thunk,logger);

export const store = createStore(reducers,middleware);