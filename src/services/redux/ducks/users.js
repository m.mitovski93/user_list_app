const ADD = 'ADD';
const DELETE = 'DELETE';
const EDIT = 'EDIT';

const initState = {
    data: []
};

export const usersReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD:
            return { ...state, data: [...state.data, action.payload] };

        case DELETE:
            return { ...state, data: [...state.data.filter((u) => u.id !== action.payload)] }

        case EDIT:
            const users = state.data.map(u => {
                if (u.id === action.payload.id) {
                    u = action.payload
                };
                return u
            });
            return { ...state, data: users };

        default:
            return state;
    }
};

const setAddUser = (data) => {
    return {
        type: ADD,
        payload: data
    };
};

const setDeleteUser = (id) => {
    return {
        type: DELETE,
        payload: id
    };
};

const setEditUser = (data) => {
    return {
        type: EDIT,
        payload: data
    };
};

export const addUser = (data) => {
    return (dispatch) => {
        dispatch(setAddUser(data))
    };
};

export const deleteUser = (id) => {
    return (dispatch) => {
        dispatch(setDeleteUser(id))
    };
};

export const editUser = (data) => {
    return (dispatch) => {
        dispatch(setEditUser(data))
    };
};