export const sanitaze = (data) => {
    for (let key in data) {
        if (typeof data[key] === 'string') {
            data[key] = data[key].trim();
        }
        if (!data[key]) {
            return false;
        };
    };
    return true;
};