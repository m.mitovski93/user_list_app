import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Dashboard from "./com/pages/Dashboard"
import './assets/styles/global.css'

const App = () => {
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route path='/' element={<Dashboard />}>
                    </Route>
                </Routes>
            </Router>
        </div>
    );
};

export default App;
